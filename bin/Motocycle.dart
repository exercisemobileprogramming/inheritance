import 'RunObject.dart';
import 'Vehicle.dart';

class Motocycle extends Vehicle with RunObject{
 
  Motocycle(String name,String brand,String color,int wheel,num maxSpeed,int? pistons,num? Fuel) 
  : super(name,brand, color, wheel, maxSpeed, pistons,Fuel);
  
  void Run(num speed){
    if(speed>=0&&speed<=maxSpeed){
      print("$brand $name is running $speed km/h.");
    }else if(speed<0){
      print("$brand $name is can not running $speed km/h.");
      print("(Speed must >0 and <= $maxSpeed).");
    }else{
      print("$brand $name can't runnig to $speed km/h because maxspeed this motocycle is $maxSpeed.");
    }
  }

  void startEngine(){
    if(super.haveEngine()){
      print("This motocycle $name is start with $pistons pistons");
    }else{
      print("This motocycle $name is can't start engine because this motocycle didn't have engine.");
    }
  }
 
}