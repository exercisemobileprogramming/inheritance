import 'Vehicle.dart';
import 'Motocycle.dart';
import 'RunObject.dart';
import 'Car.dart';
import 'Boat.dart';
import 'SwimObject.dart';



void main() {
  Motocycle R6 = new Motocycle("R6", "Yamaha", "blue", 2, 285,4,10);
  R6.move();
  R6.Run(300);
  R6.startEngine();
  Car GTR = new Car("GTR","Nissan","black",4,400,6,20);
  print("\n");
  GTR.move();
  GTR.Run(-1);
  GTR.Run(399);
  GTR.startEngine();

 Boat AzimutYachts = new Boat("Azimut 50", "Azimut Yachts", "White", null, 59.264, 6, 4000);
 print("\n");
 AzimutYachts.move();
 AzimutYachts.swim(-1);
}
