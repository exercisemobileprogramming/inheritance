abstract class Vehicle {
  late String name;
  late String brand;
  late String color;
  int? wheel;
  late num maxSpeed;
  int? pistons;
  num? Fuel; 

  Vehicle(String name,String brand,String color,int? wheel,num maxSpeed,int? pistons,num? Fuel){
    this.name=name;
    this.brand=brand;
    this.color=color;
    this.wheel=wheel;
    this.maxSpeed=maxSpeed;
    this.pistons=pistons;
    this.Fuel=Fuel;

  }

  void move(){ print("$brand $name can move with $wheel wheels"); }

  bool haveEngine(){
    if(Fuel!=null&&pistons!=null){
      return true;
    }
    return false;
  }
}